#Minigame with ressources

## Metals

_N1_
You have to check the fusion point of the metal.
The copper's color change and the player have to click on it at the right time.
Too soon or to late lead to a faillure

_N2_
You have a iron ingot. Six points appears on it during a limited time.
The player have to click on it at the right time or else it's a faillure.

_N3_
The player have to spill liquid gold in a mold.
If the spill it outside the mold, you loose.

_N4_
The player have to type a word between the hammer's hits.
The tempo accelarate with time.

_N5_
A piece of metal is stuck in a rock.
The player have to hit pricisely on the rock to obtain the metal in a limited amount of time.

_N6_
The player have to fill his forge with coal.
He must tapes quicly the letters that appears on screen.

_N7_
A wheel with a mark on it spin faster and faster.
The player have to press space to place his item on it. This fill a bar the player must complete it.
The bar is drain over the time.
But be aware of the mark, if it touch the item it will damage the item and reduce the bar.
If the bar is empty it's a loose.

_N8_
The player have to catch a hot iron ingot. The ingot bounce he him [3-5] times.


## Woods

_N1_
A tree cut itself. Branch can spaw in each side. The player have to dodge the branch.
Press space to change your side. If a branch touch the player, he loose.
You have to dodge [5-8] times to win.

_N2_
There is a bar. A cursor go up-and-down and you have to press space when the cursor is in a certain area.
The player have to do it [4-7] times to win. If he fail, he loose.

_N3_
You have a piece of wood which you have to carve in a precise form.
To do it you have to touch saw up and down on the screen during a slidescrolling.
If in the end you didn't have the correct form, you loose.

_N4_
You have to carve a piece of wood.
The player have to use the mouse to carve vertically and use the arrows to rotate the piece.
Limited ammount of time.

_N5_
To was through a tree the player have to type "right" and "left" again and again.
Limited amount of time.

_N6_
A trunk flow in a river. The player controll it and have to dodge obstacles.

_N7_
A tree have banchs composed of words. The player have to type the words to cut the branchs.

_N8_
The chunk of wood advance. The player have to press start at the right time to cut it.