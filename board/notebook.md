[jam](https://itch.io/jam/world-mytholojam)

# Idées / Concepts

Quelques idées et concepts de jeu pour la gamejam

## Rogue-Like

*Binding of Isaac like*

Labyrinthe généré aléatoirement. Plusieurs niveaux. Battre un boss pour terminer un niveau.  

Système de ressource (or) et d'item (récompense de boss et shop).  

Thème grec (le Dédale, Thésée, le minotaure).

Système semblable au Nemesis de Resident Evil 3 ou Mr.X de Resident Evil 2 Remake : Le minotaure apparaît et chasse le joueur au bout de x salles d'un niveau.

Idées en vrac : Icare et Dédale tiennent la boutique / Boss = monstre antique / Deux sorties par niveau : une normale et une avec le fil d'Ariane (Bad et good end).

*Darkest Dungeon like*

Composer une équipe de trois dieux et partir dans des "donjons" avec. Donjon généré aléatoirement.

Système de RPG, chaque dieu peut gagner en niveau (meilleures stats et nouvelles compétences).

Stats : 
* PV
* Défense (réduit les dégâts)
* Attaque (dégâts infligés)
* Précision (chance de toucher) 
* Pureté (pour la corruption, voir plus bas)
* Chance (coups critiques)

Un dieu ne peut s'équiper que de 3 compétences avant un donjon (chaque dieu aurait un pool de 6 ou 9 compétences)

Système d'équipement (arme, armure, trinket).

Placement important (Front et back line, certains dieux sont spécialisés, d'autres plus versatiles).

Système d'équipe : suivant la composition du groupe de dieu bonus ou malus (ex : panthéon grec = +pureté / dieux de la guerre = +attaque / dieux ennemis = -gain d'xp).

4 rôles : Tank, DPS, Healer, Debuffer

Double système de vie : PV (arrivé à zéro le dieu disparaît du donjon, perd un niveau et son équipement) et Corruption (arrivé à zéro le dieu passe à l'ennemi, permadeath).

Peut utiliser toutes les mythologies / Idées d'anciens dieux vs nouveaux dieux (internet, télévision, consumérisme) (voir American Gods de Neil Gailman)

Exemples de dieux :
* Arès [Tank]
PV : 10/10
Pureté : 10/10
Défense : 8
Attaque : 5
Précision : 75%
Chance : 15%
Compétences : Shield (augmente la défence des dieux en frontline) / Push (pousse un ennemi de la frontline à la backline) / Bash (assome un ennemi)

* Loki [Debuffer]
PV : 8/8
Pureté : 8/8
Défense : 4
Attaque : 8
Précision : 80%
Chance : 35%
Compétences : Taunt (force l'ennemi à l'attaquer) / Leurre (crée une copie de Loki, les ennemis attaquent le leurre avant lui) / Poison (2PV par tour sur l'ennemi touché)

* Anubis [Healer]
PV : 8/8
Pureté : 12/12
Défense : 6
Attaque : 4
Précision : 75%
Chance : 15%
Compétence : Soin (soigne pv) / Purification (soigne pureté) / Drain de vie (attaque un ennemi et soigne le dieu d'un montant égal aux dégâts).

## Point&Clic

> Day of tentacle

Jeu d'aventure lié aux horraires.

Mythe de Janus ? Idée de deux intrigues parallèles, une le jour et une autre la nuit. Passé/Futur

Suivant l'heure à laquelle on joue, on incarne un personnage différent.

Voir pour des puzzles se réalisant en communication entre le jour passé et la nuit future. (ex : poser un objet le jour qui popera donc au même endroit la nuit).

## Gestion


Hephaïstos Forge

On incarne le dieu Hephaïstos et l'on doit produire des armes, armures et artéfacts pour les dieux et héros.

Système de commande, un client vient avec ses envies (ex: je veux une arme qui fasse mal aux mort-vivants et rapidement)

Le joueur doit produire un objet le plus proche possible des critères :
* Type d'objet : Arme, armure, artéfact
* Qualité : Moyenne, Bonne, Exceptionelle, Légendaire
* Temps : Rapide, Moyen, Long
* Effet : Au hasard parmi un pool de nombreux effets

On doit donc pour réussir jauger des besoins du client mais aussi avoir de la chance.

## Tactical tour par tour

*Advance Wars like*

Jeu de guerre tactique au tout par tour où l'on incarne un panthéon.

Il faut se battre contre les autres panthéons. Chaque panthéon à sa spécialité et des unités spécifiques.

Système de ressource durant les batailles permettant de lancer des sorts.

Permadeath des unités. Gagner des batailles donne de l'or permettant d'acheter de nouvelles unités ou d'améliorer celle que l'on a déjà.

Système de RPG : Chaque panthéon possède trois dieux qui servent de généraux. Le joueur doit en choisir un par bataille, chacun ayant des effets passifs et sorts différents.

Exemple de panthéon : Grec

Généraux : 
* Zeus, spécialisé dans les sorts faisant des dégâts directement sur les ennemis
* Arès, augmente les dégâts qu'infligent les unités alliées
* Athéna, augmente les déplacements des unités

Unités :
* Hoplites : efficace contre cavalrie
* Pégases : peut surmonter les obstacles
* Satyres : à distance, bon mouvement
* Hydre : régen de pv par tour
* Minotaure : long mouvement en ligne droite

