// # Ressources

var r = {}

// ## Materials 

r.m = {}

r.m.metal       = { // efficiency
    "copper"    : 0,
    "iron"      : 1,
    "gold"      : 2
}

r.m.wood        = { // durability
    "oak"       : 0,
    "cedar"     : 1,
    "ebony"     : 2
}

// ## Weapons

var r.w = {}

r.w.dagger = ["wood","metal"]
// q 0-2 r 0-2

r.w.targe = ["wood","metal","wood"]
// q 0-2 r 0-4

r.w.axe = ["wood","wood","metal"]
// q 0-2 r 0-4

r.w.sword = ["wood","metal","metal"]
// q 0-4 r 0-2

r.w.mace = ["metal","metal","metal"]
// q 0-6 r 0

r.w.shield = ["wood","wood","wood"]
// q 0 r 0-6

r.w.great_axe = ["wood","wood","metal","metal"]
// q 0-4 r 0-4

r.w.great_sword = ["wood","metal","metal","metal"]
// q 0-6 r 0-2

r.w.spear = ["wood","wood","wood","metal"]
// q 0-2 r 0-6

r.w.great_shield = ["wood","wood","wood","wood"]
// q 0 r 0-8

r.w.war_hammer = ["metal","metal","metal","metal"]
// q 0-8 r 0


// ## Faction

r.f = {}

r.f.Athens = {
    ressource : "oak",
    value : 1,
}

r.f.Rhodes = {
    ressource : "copper",
    value : 1,
}

r.f.Corinth = {
    ressource : "cedar",
    value : 2,
}

r.f.Sparta = {
    ressource : "iron",
    value : 2,
}

r.f.Thebes = {
    ressource : "gold",
    value : 3,
}

r.f.Olympia = {
    ressource : "ebony",
    value : 3,
}

// ## PNJ

r.p = {}

// Every factions is link to each tier ressource through a character, so basicallly, each faction have three chracters who is shared with another faction.

r.p.Leonidas = {
    attack  : 8, // bonus to quality during fight
    defence : 2,  // bonus to raw during fight
    factions : ["Sparta","Olympia"]
}

r.p.Theseus = {
    attack  : 3,
    defence : 7,
    factions : ["Athens","Thebes"]
}

r.p.Cypselus = {
    attack  : 5,
    defence : 5,
    factions : ["Corinth","Sparta"]
}

r.p.Bellerophon = {
    attack  : 7,
    defence : 3,
    factions : ["Thebes","Rhodes"]
}

r.p.Heracles = {
    attack  : 9,
    defence : 1,
    factions : ["Olympia","Rhodes"]
}

r.p.Diagoras = {
    attack  : 4,
    defence : 6,
    factions : ["Rhodes","Athens"]
}

r.p.Pericles = {
    attack  : 6,
    defence : 4,
    factions : ["Athens","Sparta"]
}

r.p.Diogenes = {
    attack  : 1,
    defence : 9,
    factions : ["Corinth","Thebes"]
}

r.p.Pelopidas = {
    attack  : 2,
    defence : 8,
    factions : ["Thebes","Olympia"]
}

// # History

var history = {
    days : []
}



// ## Quests

var quest = function(pnj,weapon){
    this.pnj = pnj
    this.weapon = weapon
    this.position = false
}

// # Crafting

var player = function(){
    this.fatigue = 0
    this.craft = []
    this.workshop {
        stock : {
            "copper" : 5,
            "oak"   : 5
        }
    }
}

player.prototype.hammer = function(ressource){
    if(!this.workshop[ressource]) return false
    this.workshop[ressource] --
    return (mini_game[ressource]() ? this.craft.push(ressource) : false)
}

player.prototype.reply = function(quest){
    var response = false
    if ( this.craft.length == quest.weapon.length ) {
        response = true
        for ( var i = 0; i < quest.weapon.length; i++ ){
           if(!r.m[quest.weapon[i]][this.craft[i]]) {
               response = false
               break
           }
        }
    }
    this.craft = ""
    if ( response ) {
        quest.pnj.weapon = {
            "quality"   : 0,
            "raw"       : 0
        }
        for ( var i = 0; i < this.craft.length; i++ ){
            if(r.m.metal[this.craft[i]]) quest.pnj.weapon.quality += r.m.metal[this.craft[i]]
            if(r.m.wood[this.craft[i]]) quest.pnj.weapon.raw += r.m.metal[this.craft[i]]
        }
    } 
    return response
}

// # FIGHT

function fight(pnj1,pnj2){
    pnj1.weapon.quality + pnj1.attack > pnj2.weapon.raw + pnj2.defence ? true : false
}

// # GAMES

games = {
    "copper"    : function(){return true},
    "iron"      : function(){return true},
    "gold"      : function(){return true},
    "oak"       : function(){return true},
    "cedar"     : function(){return true},
    "ebony"     : function(){return true}
}

function timber(time,speed,index,tree){
    if (!index) index = 0
    if (index == time) return true
    if (!tree) {
        tree = []
        for ( var i = 0; i < time; i++ ) {
            tree.push (Math.random() > 0.5 ? true : false)
        }
    }
    setTimeOut(function(){
        if(!(player.position == tree[index] ? true : false)){
            return false
        }else{
            return timber(time,speed,index+1,tree)
        }
    },speed)
}
