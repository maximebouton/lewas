# Factions

## Greek Factions

### Sparta
- Ressource : Iron
- God : Ares
- PNJs : Leonidas / Cypselus / Pericles

### Athens
- Ressource : Oak
- God : Athena
- PNJs : Theseus / Diagoras / Pericles

### Corinth
- Ressource : Cedar
- God : Poseidon
- PNJs : Cypselus / Diogenes

### Thebes
- Ressource : Gold
- God : Hades
- PNJs : Diogenes / Bellerophon / Theseus

### Rhodes
- Ressource : Copper
- God : Helios
- PNJs : Bellerophon / Heracles / Diagoras

### Olympia
- Ressource : Ebony
- God : Zeus
- PNJs : Heracles / Pelopidas / Leonidas

## Egyptian Factions

### Iunu
- Ressource : Sand / Iron
- God : Ra
- PNJs :
- Relation :

### Zawty
- Ressource : Souls / Wood
- God : Anubis
- PNJs :
- Relation :

### Men-nefer
- Ressource : Leather / Gold
- God : Ptah
- PNJs :
- Relation :

## Nordic Factions

### Hedeby
- Ressource : Fur / Leather
- God : Odin
- PNJs :
- Relation :

### Uppakra
- Ressource : Iron / Stone
- God : Thor
- PNJs :
- Relation :

### Birka
- Ressource : Wood / Gold
- God : Freya
- PNJs :
- Relation :

