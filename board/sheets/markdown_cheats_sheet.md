# Titrage

Titre 1
=======

## Titre 2

Titre 2
-------

## Mise en forme

saut de ligne = nouveau paragraphe

exemple paragraphe 1
exemple paragraphe 1

    <p>exemple exemple1</p>

exemple paragraphe 2

exemple paragraphe 2

    <p>exemple paragraphe 2</p>
    <p>exemple paragraphe 2</p>

blabla `inline code` blabla

    <p> blabla <code>inline code</code> blabla </p>

    bloc de code, ligne 0
    bloc de code, ligne 1
    bloc de code, ligne 2
    ect…

_italic_

    <i>italic</i>

__bold__

    <strong>bold</strong>

## listes

- item de liste 1
- item de liste 2
- item
- item

+ ça marche aussi
+ item
+ item
+ item

* pareil
* item
* item

## sous-listes

- item 1
  * item 1a
  * item 1b
- item 2
  - item 2a

## Citations

> il n'y a pas de plus belle chose que le markdown
> c'est vraiment bien

_Michel citation, sur le markdown, 1987, gallipette_

    <blockquote> il n'y a pas de plus belle chose que le markdown c'est vraiment bien </blockquote>

## notes et références 

blabla c'est lui[^1] qui à dit blabla pour la première fois[^2]

[^1]: Michel référencé, 1984, éditions du minou.
[^2]: ça se discute

## liens

[titre](url)

[pour en savoir plus sur le markdown](https://github.com/adam-p/markdown-here/wiki/Markdown-Cheatsheet)
