## initialiser un git

va dans un dossier pré-existant ou nouveau et tape

    git init

## cloner un git existant

    git clone <url.git> <nom du dossier>

---

_quoi faire après avoir modifier ajouter des fichiers dans le git (faire un commit et le pousser sur le git)_

## 1. État du git

dans le dossier git tape :

    git status

## 1(bis). Tirer sur le git distant

si il y a eu des push d'autres personnes sur le git

    git pull origin master

## 2. Ajouter des changements au commit

toujours dans le dossier tape :

pour une modification ou un ajout spécifique :

    git add <file>

pour tout ajouter :

    git add -A 

annuler une modification

    git checkout <file>

## 3. Vérifier que les changements ont bien été pris en compte

    git status

si tu veux revenir en arrière sur une <file> particulière

    git rm --cached <file>

## 4. Faire un commit

c'est à dire une version du logiciel

    git commit -m"ton commentaire sur la version"

## 5. Pousser sur le git (branche principale)

avant de pousser il faut toujours tirer (voir 1bis), le logiciel t'empeche de pousser si ta version est pas à jour

    git push origin master


