
# System Draft
Some ideas for the system game

### Object Limit
The object have a limit of completion.

Quality chart :
100 = Good (no bonus)
150 = Great (+50% Reputation)
200 = Legendary (+100% Reputation)

### Reputation System
The player have a reputation with each customer.
The reputation go from 0 to 10.
Each 3 sucessful items delivered to a customer add 1 reputation with him.
At 3 / 5 / 7 / 9 / 10 client give a special quest.

The reputation with the faction = the reputation with all members of this faction
5 / 10 / 15 / 20 = +3 ressources at each trade.

### Command System
A command is defined by his difficulty
More a command is hard and less the player have time to complete it.

Difficulty chart :
- Trivial
- Simple
- Moderate
- Challenging
- Arduous
- Godly
- Mythic

<i>Trivial</i>
Time : 40
RP : 1
XP : 1

<i>Simple</i>
Time : 35
RP : 2
XP : 1

<i>Moderate</i>
Time : 30
RP : 2
XP : 2

<i>Challenging</i>
Time : 25
RP : 3
XP : 2

<i>Arduous</i>
Time : 20
RP : 3
XP : 3

<i>Godly</i>
Time : 15
RP : 4
XP : 3

<i>Mythic</i>
Time : 10
RP : 5
XP : 5
 
### Energy System
The player have energy, a ressource that limit his action.
1 hammer strike = 1 exhaust point

The player, at the begining, have a limit of 8 exhaust points.
If the player reach that limit he pass out, spending 3 times points.
Sleeping reset the exhaust points and spend just 1 time point.

### Time System
A command have a time limit.
If the object isn't competed at the end of the limit = FAIL
If the Player give a complete object before the time limit = XP bonus

### XP System
Succeed a command give experience points (XP) to the Player.
Each 5*Player lvl xp = Level up
<strong>Level Up</strong>
When lveling up the player can upgrade his forge
- Consitution = Fatigue +
- Strengh = Hammer +
- Charisma = Reputation +
- Wisdom = Time limit +
- Intelligence = XP +


### Minigame System
Each hammer strike activate a minigame.


### Faction System
Each client/PNJ is part of a faction. The player have a certain reputation with each faction, his actions change this reputation.
If a player help a faction his reputation with it will be better.
If a player doesn't help a faction his reputation with it will be worst.

A player can make a request to a faction to have ressources. If he do so then he have to make a promess to this faction.
Respecting the promess give the player RP.
If the player don't prespect his promess then he loose RP and his reputation with this faction reduce.

### Blueprints
To craft a weapons, artefact or anything you have to know his blueprint. Player can guess with try and retry some of them.
But he can also gain blueprints with sucefull quest or as a gift from good friends.
The blueprints, named "Ancient Slab", are crypted in a binary langage (the langage of the gods).
Because of this player as to learn this langage (with essay, with a traductor outside the game or learning some words with quests) to understand them.

_Example of blueprint_

Sword : wood / metal / metal

Ancient slab sword : 

01110111 01101111 01101111 01100100

01101101 01100101 01110100 01100001 01101100

01101101 01100101 01110100 01100001 01101100

The gods speaks in binary too, so to understand them the player have to learn this langage.

### Crafting System

The player have to travel across a map like Super Mario. Each tape of his travel his a minigame.
The player have to choose wich ressource he have to bet before knowing the minigame he have to play.
When it's the first time that the player craft a item the map is covered by a fog.

---

    2019-04-07, gameplay elements

* factions
  + clients

* materials
  + sub-metarials

* items
  + effeciency (attack)
  + durability (defense)

* events
  + quests
    - global_event
    - characters_events
    - generic_events 

* interface
  + workshop
  + craft_map
  + calendar
  + reputation_map
  + stock
  + blueprints
  + archives ?
  + quests

* craft
  + recipes
  + templates
    - area
      1. glyph
        2. input
        2. output
        2. generic
  + demands
  + mini-games
    - games
    - narratives
