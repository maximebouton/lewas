// ## ITEMS

var i = {}

var metal       : { // quality
    "cooper"    : 0,
    "iron"      : 1,
    "gold"      : 2
}

var wood        : { // raw
    "oak"       : 0,
    "cedar"     : 1,
    "ebony"     : 2
}

i.spear = ["metal","wood","wood","leather"]

i.shield = {
    "wood":     4,
    "iron":     2,
    "leather":  2
}

i.helmet = {
    "iron":     4,
    "leather":  4
}

i.thunder = {
    "soul":     999
}

// ## PNJ
// 
// ### GODS
//
// #### GREEKS
// 
// var ares = new pnj(
//     /*name*/    "Ares",
//     /*rank*/    "god",
//     /*items*/   ["spear","shield","helmet"]
// )
// 
// var athena = new pnj(
//     /*name*/    "Athena",
//     /*rank*/    "god",
//     /*items*/   ["spear","shield","helmet"]
// )
// 
// var zeus = new pnj(
//     /*name*/    "Zeus",
//     /*rank*/    "god",
//     /*items*/   ["thunder"]
// )
// 
// var poseidon = new pnj(
//     /*name*/    "Poseidon",
//     /*rank*/    "god",
//     /*items*/   ["trident"]
// )
// 
// var hades = new pnj(
//     /*name*/    "Hades",
//     /*rank*/    "god",
//     /*items*/   ["boat"]
// )
// 
// var apollo = new pnj(
//     /*name*/    "Apollo",
//     /*rank*/    "god",
//     /*items*/   ["bow"]
// )
// 
// var helios = new pnj(
//     /*name*/    "Helios",
//     /*rank*/    "god",
//     /*items*/   ["spear","shield","helmet"]
// )
// 
// HEROES

var leonidas = new pnj(
    /*name*/    "Leonidas",
    /*rank*/    "hero",
    /*items*/   ["spear","shield","helmet"]
)

var theseus = new pnj(
    /*name*/    "Theseus",
    /*rank*/    "hero",
    /*items*/   ["spear","shield","helmet"]
)

var Cypselus = new pnj(
    /*name*/    "Cypselus",
    /*rank*/    "hero",
    /*items*/   ["spear","shield","helmet"]
)

// ## Factions

// ### GREEKS

var sparta = new faction(
    /*name*/            "Sparta",
    /*ressource*/       "iron",
    /*value*/           10
)

var athens = new faction(
    /*name*/            "Athens",
    /*ressource*/       "wood",
    /*value*/           5
)

// var corinth = new faction(
//     /*name*/            "Corinth",
//     /*ressource*/       "gold",
//     /*value*/           100
// )
// 
// var thebes = new faction(
//     /*name*/            "Thebes",
//     /*ressource*/       "leather",
//     /*value*/           50
// )
// 
// var rhodes = new faction(
//     /*name*/            "Rhodes",
//     /*ressource*/       "copper",
//     /*value*/           20
// )
// 
// var olympia = new faction(
//     /*name*/            "Olympia",
//     /*ressource*/       "stone",
//     /*value*/           1
// )

sparta.pnjs = [leonidas]
athens.pnjs = [theseus]

console.log("data.js")
