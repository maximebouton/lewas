// # common

var factions = [], battles = []

// # battle

battle = function(day){
    this.name
    this.day = day
    this.offender = 
    this.opponent = 
    this.quests = []
}

battle.prototype.quest = function(){
    new quest()
}

// # demands

demand = function(){
    this.client
    this.raw
    this.quality
    this.aid
    this.item
    this.reward
}

// # quests
//

function quest(pnj,item,reward) {
    this.client
    this.item
    this.reward
}

quest.prototype.reply = function(bool){
    if(bool){
        new order(this)
    }else{
        quest.remove()
    }
}

// # order
//

order = function() {
    this.quest
}

order.prototype.deliver = function(){
}

// # item

item = function(){
    name
    quality
    raw
}

item.prototype.hold = function(){
}

item.prototype.hammer = function(){
}

item.prototype.break = function(){
}

// # pnj

Pnj = function(name,rank,items){
    this.name = name
    this.rank = rank
    this.items = items
}

// # faction

faction = function(name,ressource,value){
    this.name = name
    this.ressource = ressource
    this.value = value
    this.pnjs = []
}

faction.prototype.barter = function(){
}

faction.prototype.visit = function(){
}

faction.prototype.event = function(attaquants,defenders){
    new event()
}

// # player
//

Player = function(name){
    this.name = name
    this.power = 0
    this.fatigue = 0
}

Player.prototype.rest = function(){
    round();
}

// # workshop

Workshop = function(){
    this.name
    this.items = {}
    this.stock = {}
    this.size = 0
}

Workshop.assemble = function(){
    new item
}

console.log("objets.js")
