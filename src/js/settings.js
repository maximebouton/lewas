// # Settings

console.log("settings.js")

var s = {}

// ## items

s.i = {
    "bow" : {
        name : "bow",
        quality : 0,
        raw : 100,
    },
    "spear" : {
        name : "spear",
        quality : 0,
        raw : 100,
        // attack : 5,
        // defense : 0,
        // speed : -5,
        // pv : 0
    }
}

// ## rewards

s.r = {
    "nothing" : {
        name : "nothing",
    },
    "silver" : {
        name : "silver",
    }
}

// ## pnj

s.pnj = {
    "Xerxes":{
        name : "Xerxes",
        reputation : 0,
        pv : 2,
    },
    "Leonidas":{
        name : "Leonidas",
        reputation : 0,
        pv : 2,
        // attack : 5,
        // defense : 5,
        // speed : 5,
        // pv : 5,
        // faction : "Sparta"
    }
}

// ## factions

s.f = {
    "Sparta" : {}
}

// ## defaults

var d = {}
