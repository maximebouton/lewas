// # interface

console.log("interface.js")

var negatif  = ["below","tired","exausted","dead",":(","Invalid","invalid","refuse","false","Fail","fail","removed","remove",'-',"improperly"]
var choice  = ["Info","List","Select","Help"]
var positif  = ["above","proudly","amazing","strong","fine","New","new","accept","true","pleased","very pleased","Success","success",'+']
var object   = ["client","order","quest","clients","orders","quests","item"]
var variable = ["quality","raw","progress"]
var fn  = ["drop","deliver","hammer","hold","reply"]

function echo(where,str,type,speed){
    if(!speed) speed = 50
    var pre = document.createElement(type)
    if ( where == commands || where == workshop ){
        where.insertBefore(pre,where.children[1]) 
    }else{
        where.insertBefore(pre,where.children[where.children.length-1])
    }
    var tokens = str.split(' ')
    
    for(var i = 0; i < tokens.length; i++ ){
        var span = document.createElement('span')
        if(parseInt(tokens[i][tokens[i].length-1]) >= 0 
            || parseInt(tokens[i][tokens[i].length-1]) <= 0){
            span.classList.add("value")
        }
        if(s.pnj[tokens[i]]) span.classList.add("pnj")
        if(s.i[tokens[i]]) span.classList.add("item")
        if(s.r[tokens[i]]) span.classList.add("reward")
        if(positif.indexOf(tokens[i]) > -1) span.classList.add("positif")
        if(negatif.indexOf(tokens[i]) > -1) span.classList.add("negatif")
        if(object.indexOf(tokens[i]) > -1) span.classList.add("object")
        if(variable.indexOf(tokens[i]) > -1) span.classList.add("variable")
        if(fn.indexOf(tokens[i]) > -1) span.classList.add("fn")
        if(choice.indexOf(tokens[i]) > -1) span.classList.add("choice")
        if(choices[tokens[i]]) span.classList.add("shortcut")
        pre.appendChild(span)
        animText(span,tokens[i],parseInt(Math.random()*speed))
        var span = document.createElement('span')
        if(i == tokens.length-1) continue
        span.textContent = ' '
        pre.appendChild(span)
    }

}

function animText(el,str,speed){
    if(!str.length) {
        if ( el.parentNode.parentNode != commands ){
            el.parentNode.parentNode.scrollTop = el.parentNode.parentNode.scrollHeight;
        }
        return
    }
    el.textContent += str[0]
    setTimeout(function(){
        animText(el,str.substr(1),speed)
    },speed)
}

function anim(where,name,frames,speed,el,i){
    if(!speed) speed = 200
    if(!i)i=0
    if(i > frames) return el.parentNode.removeChild(el)
    if(!el) el = document.createElement("pre")
    fetch("./src/anim/"+name+i+".txt")
    .then(response => response.text())
    .then(function(text){
        el.textContent = text
        where.insertBefore(el,where.children[where.children.length-1])
        el.parentNode.scrollTop = el.parentNode.scrollHeight;
        setTimeout(function(){
            anim(where,name,frames,speed,el,i+1)
        },speed)
    })
}

function print(where,name){
    var pre = document.createElement("pre")
    fetch("./src/anim/"+name+".txt")
    .then(response => response.text())
    .then(function(text){
        var tokens = text.split('\n')
        for(var i = 0; i < tokens.length; i++ ){
            var span = document.createElement('span')
            pre.appendChild(span)
            animText(span,tokens[i],parseInt(5+Math.random()*10))
            var span = document.createElement('span')
            if(i == tokens.length-1) continue
            span.textContent = '\n'
            pre.appendChild(span)
        }
        where.scrollTop = where.scrollHeight;
    })
    where.insertBefore(pre,where.children[where.children.length-1]) 
}

var option = function(){return false}

choices['m0'] = function(){
    list.display()
}

choices["h2"] = function(){
    echo(menu_log,"Help",'p')
    echo(menu_log,"An order need a quality > raw to be delivered, to increase order progress you need to work on it",'p')
    echo(menu_log,"First you need to hold your order item during a certain amount of --time, limited by your level capacity",'p')
    echo(menu_log,"e.g, o00 15, hold order 0 item for 15 as --time",'p')
    echo(menu_log,"During this time you need to hammer your order item, to aim correctly you need to find a --key",'p')
    echo(menu_log,"e.g, o01 coal, hammer order 0 item with coal as --key",'p')
}

choices["--time"] = function(){
    echo(menu_log,"--time is a command parameter providing a duration in seconds, e.g 15",'p')
}

choices["--key"] = function(){
    echo(menu_log,"--key is a command parameter providing a password, e.g coal",'p')
}

function choose(value){
    if ( choices[value.split(' ')[0]] ) return choices[value.split(' ')[0]](value)
    if ( option(value) ) return
    echo(commands,"Invalid command :( ",'p')
}

var list = {
    "display" : function(){
        echo(menu_log,"List",'p')
        for ( var i = 0; i < list.option_name.length; i++){
            choices['l'+i] = function(value){
                list.trigger(value[1])
            }
            echo(menu_log,'l'+i+' '+list.option_name[i],'p')
        }
    },
    "option_name" : ["clients","quests","orders"],
    "option"  : [clients,quests,orders],
    "trigger" : function(o){
            var removed = 0 
            for ( var i in list.option[o] ) if ( list.option[o][i].removed ) removed++
        console.log(list.option[o].length,removed)
            if ( !list.option[o].length || removed == list.option[o].length ){
                return echo(menu_log,"you donʼt have "+list.option_name[o],'p')
            }
            echo(menu_log,"Select",'p')
            for ( var i in list.option[o] ) {
                if(!list.option[o][i].removed) list.option[o][i].display() 
            } 
        echo(menu_log,"m0 display list menu",'p')
    }
}
/*
    "select" : function(i) {
       // selected_obj = 
        // 0
    },
    "quest" : {
        "reply" : {
            "true" : function() {},
            "false" : function() {}
        }
    },
    "order" : {
        "hammer" : function() {
        },
        "hold" : function() {
        }
    }
}*/

var input = document.createElement("input")
var commands = document.getElementById("commands")
var menu_log = document.getElementById("menu")
commands.appendChild(input)

var index = 0

function getHistory(el,index,down,i){
    if(down){
        if(el.nextSibling.textContent == el.textContent) {
            index --
            return getHistory(el.nextSibling,index,down,i-1)
        }
    }else{
        if(el.nextSibling.textContent == el.textContent) {
            index ++
            return getHistory(el.nextSibling,index,down,i+1)
        }
    }
    if(!i) i = 0
    if(i<index){
        return getHistory(el.nextSibling,index,down,i+1)
    }
    return el.nextSibling.textContent
}

window.addEventListener("keydown",function(e){
    if(e.key =="ArrowUp"){
        input.value = getHistory(input,index)
        index++
    }
    if(e.key =="ArrowDown"){
        input.value = getHistory(input,index-2,true)
        index--
    }
    if(e.key == "Enter"){
        index = 0
        var value = input.value
        input.value=''
        try {
            echo(commands,value,"p",1)
            choose(value)
            //eval(input.value)
        }catch(e){
            console.log(e)
        }
    }
    input.focus()
})
