// # Game-play

console.log("game_play.js")

var order_index = 0
var clients = []
var quests = []
var orders = []

// ## workshop

var work_stat = {
    fatigue : 1,
    force : 30,
}

var day = 0;

var fatigue = ["amazing","strong","fine","normal","tired","exausted","dead inside"]

function feels(value){
    var feel_value = parseInt(work_stat.fatigue*fatigue.length/work_stat.force)
    var feel = fatigue[feel_value]
    echo(menu,"w0 to rest",'p')
    if ( feel == undefined ) feel = "dead inside"
    return feel
}
choices = {}
choices["w0"] = function(){sleep()}

function sleep(){
    work_stat.fatigue = 1;
    day++;
    echo(events,"Day "+day,"p")
    echo(events,"You sleep well without having dreams",'p')
    echo(events,"You feel "+feels(work_stat.fatigue),"p")
    for(var i = 0; i < quests.length; i++){
        if(quests[i].type == "order" && !quests[i].removed){
            if(quests[i].date - day == 0){
                quests[i].client.pnj.reputation -= 1
                quests[i].removed = "refused"
                echo(events,"it is too late to reply to the quest "+ quests[i].id,"p")
                echo(workshop,quests[i].id + " removed from quests","p")
                choices[quests[i].id] = undefined
                choices[quests[i] + '0'] = undefined
                choices[quests[i] + '1'] = undefined
            }
        }
    }
    for(var i = 0; i < orders.length; i++){
        console.log(!orders[i].removed)
        if(!orders[i].removed){
            if(orders[i].quest.date - day == 0){
                orders[i].quest.client.pnj.reputation -= 2
                orders[i].removed = "refused"
                echo(events,"You fail to deliver order "+ orders[i].id +" in time","p")
                echo(workshop,orders[i].id + " removed from orders","p")
                choices[orders[i].id] = undefined
                choices[orders[i] + '0'] = undefined
                choices[orders[i] + '1'] = undefined
                choices[orders[i] + '2'] = undefined
            }
        }
    }
    if(daily_events[day]) {daily_events[day]()}else{
        var removed = 0 
        for ( var i in quests ) if ( quests[i].removed ) removed++
        for ( var i in orders ) if ( orders[i].removed ) removed++
        if ( removed == quests.length + orders.length ){
            print(view,"island")
            return echo(events,"Sucess you donʼt have nothing to do today",'p')
        }

    }
}

// ## Events

var daily_events = [
    null,
    function(){
        new Client("Leonidas")
        new Quest(0,{type:"order",item:"spear",date:4},"nothing")
    },
    function(){
        new Client("Xerxes")
        new Quest(1,{type:"order",item:"bow",date:4},"silver")
        //if(quests[0].removed == "accepted"){
        //    new Quest(1,{type:"demand",order:0,quality:50,favor:false},"gold",4)
        //}
    }
]

// ## Client

Client = function(pnj){ 
    this.id = 'c'+clients.length
    this.pnj = s.pnj[pnj]
    clients.push(this)
    choices[this.id] = function(){this.select()}.bind(this)
    echo(events,"New client "+this.pnj.name+' '+this.id,"p")
}

Client.prototype.display = function(){
    echo(menu_log,this.id+' '+this.pnj.name,"p")
}

Client.prototype.select = function(){
    echo(workshop,"Info",'p')
    for ( var i in this.pnj){
        echo(workshop,i+' : '+String(this.pnj[i]),'p')
    }
    print(view,this.pnj.name)
    echo(menu_log,"Select",'p')
    echo(menu_log,"h0 help on clients",'p') 
}

// ## Quest

Quest = function(client,options,reward){
    this.client = clients[client]
    this.reward = s.r[reward]
    this.id = 'q'+quests.length
    this.type = options.type
    quests.push(this)
    choices[this.id] = function(){this.select()}.bind(this)
    choices[this.id + '0'] = function(){this.reply(false)}.bind(this)
    choices[this.id + '1'] = function(){this.reply(true)}.bind(this)
    if(options.type == "order"){
        this.item = s.i[options.item]
        this.date = options.date
        echo(events,"New quest"+' '+this.client.pnj.name + " wants a " + this.item.name + " for  " + this.reward.name+" before day "+this.date+' '+this.id,"p")
    }
    if(options.type == "demand"){
        this.order = orders[options.order]
        this.demand = {quality:options.quality,favor:options.favor, client:this.client}
        this.order.demands.push(this.demand)
        console.log(this.order.pnj)
        echo(events,"New quest"+' '+this.client.pnj.name + " wants the " + this.order.quest.item.name + " for " + this.order.quest.client.pnj.name+' '+this.order.id+" to be a quality "+(this.demand.favor?"above":"below")+' '+this.demand.quality+' '+this.id,"p")
    }
}

Quest.prototype.display = function(){
    if(this.removed) return
    if(this.type == "order"){
    echo(menu_log,this.client.pnj.name + " want a " + this.item.name + " for " + this.reward.name+" before day "+this.date+' '+this.id,"p")}
    if(this.type == "demand"){
        echo(menu_log,this.client.pnj.name + " wants the " + this.order.quest.item.name + " for " + this.order.quest.client.pnj.name+' '+this.order.id+" to be a quality "+(this.demand.favor?"above":"below")+' '+this.demand.quality+' '+this.id,"p")
    }
}

Quest.prototype.select = function(){
    echo(menu_log,"Select",'p')
    echo(menu_log,this.id + '0 refuse quest','p')
    echo(menu_log,this.id + '1 accept quest','p')
    echo(menu_log,"h0 help on quests",'p') 
}

Quest.prototype.reply = function(resp){
    if(resp){
        echo(events,"You accept to deliver a " + this.item.name + " to " + this.client.pnj.name + " for " + this.reward.name,"p")
        this.client.pnj.reputation += 1
        anim(center,"yes",8,33)
        echo(events,this.client.pnj.name+" is pleased","p")
        echo(workshop,this.id + " removed from quests","p")
        new Order(this)
        this.removed = "accepted"
        choices[this.id] = undefined
        choices[this.id + '0'] = undefined
        choices[this.id + '1'] = undefined
    } else {
        this.client.pnj.reputation -= 1
        this.removed = "refused"
        echo(workshop,this.id + " removed from quests","p")
        echo(events,"You refuse to deliver a " + this.item.name + " to " + this.client.pnj.name + " for " + this.reward.name,"p")
        choices[this.id] = undefined
        choices[this.id + '0'] = undefined
        choices[this.id + '1'] = undefined
    }
}

Order = function(quest){
    this.id = 'o'+orders.length
    orders.push(this)
    this.quest = quest
    this.quest.item.quality = 0
    this.demands = []
    choices[this.id] = function(){this.select()}.bind(this)
    choices[this.id+'0'] = function(value){
        this.hold(value.split(' ')[1])
    }.bind(this)
    choices[this.id+'1'] = function(value){
        this.hammer(value.split(' ')[1])
    }.bind(this)
    choices[this.id+'2'] = function(value){
        this.deliver()
    }.bind(this)
    echo(events,"New order deliver a " + this.quest.item.name + " to " + this.quest.client.pnj.name + " for " + this.quest.reward.name+' '+this.id,"p")
}

Order.prototype.display = function(){
    echo(menu_log,this.id + " deliver a " + this.quest.item.name + " to " + this.quest.client.pnj.name + " for " + this.quest.reward.name,"p")
}

Order.prototype.select = function(){
    echo(workshop,"Info",'p')
    echo(workshop,this.quest.item.name +" quality "+this.quest.item.quality,'p')
    echo(workshop,this.quest.item.name + " raw " + this.quest.item.raw,'p')
    echo(menu_log,"Select",'p')
    echo(menu_log,this.id + '0 --time hold '+this.quest.item.name,'p')
    echo(menu_log,this.id + '1 --key hammer '+this.quest.item.name,'p') 
    echo(menu_log,this.id + '2 deliver '+this.quest.item.name,'p') 
    echo(menu_log,"h2 help on orders",'p') 
}

String.prototype.shuffle = function () {
    var a = this.split(""),
        n = a.length;

    for(var i = n - 1; i > 0; i--) {
        var j = Math.floor(Math.random() * (i + 1));
        var tmp = a[i];
        a[i] = a[j];
        a[j] = tmp;
    }
    return a.join("");
}

var holders = ["coal","coke","charcoal","forge","gas","anvil","hammer","chisel","tongs","fuller","hardy","tub","metal","die","heavy","impact","workpiece","material","valve","power","hydraulics","mechanics","machine","mass","force","tools","aluminum","cooper","nickel","steel","magnesium","oven","kiln","furnace","industry"]

function prog(timer){
    var r = ( 1 + work_stat.force - timer - work_stat.fatigue )
    return r > 0 ? r : 0
}

Order.prototype.hold = function(time){
    if ( work_stat.fatigue > work_stat.force/1.5){
        echo(workshop,"You canʼt hold the "+this.quest.item.name,'p')
        echo(workshop,"Fail progress - 10 ","p")
        echo(events,"You are to tired to hold the "+this.quest.item.name,'p')
        echo(events,feels(work_stat.fatigue),'p')
        this.quest.item.quality -= 10
    }
    if ( this.held ) {
        var holder = holders[parseInt(Math.random()*holders.length)]
        this.holder = holder
        var to_shuffle = holder.slice(1,holder.length-1)
        echo(workshop,"hammer --key " + holder[0]+to_shuffle.shuffle()+holder[holder.length-1],"p")
        input.value = this.id+"1 "
        return
    }
    if(!time) time = 10
    if ( time < 1 || time > work_stat.force || !parseInt(time) && time!=0 ) {
        echo(workshop,"You canʼt hold the "+this.quest.item.name+" during that amount of time",'p')
        echo(workshop,"Fail progress - 10 ","p")
        this.quest.item.quality -= 10
        return
    }
    this.held = true
    var holder = holders[parseInt(Math.random()*holders.length)]
    this.holder = holder
    var to_shuffle = holder.slice(1,holder.length-1)
    echo(workshop,"hammer --key " + holder[0]+to_shuffle.shuffle()+holder[holder.length-1],"p")
    echo(events,"you hold the "+this.quest.item.name,"p")
    this.timer = time
    setTimeout(function(){
        work_stat.fatigue += 1
        this.held = false
        echo(events,"you drop the "+this.quest.item.name,"p")
        echo(workshop,this.quest.item.name +" quality - "+(prog(this.timer) + work_stat.fatigue),"p")
        this.quest.item.quality -= prog(this.timer) + work_stat.fatigue 
        echo(workshop,this.quest.item.name +" quality is now "+this.quest.item.quality,"p")
        this.quest.item.raw -= prog(this.timer) + work_stat.fatigue
        echo(events,"you feel " + feels(work_stat.fatigue),'p')
        if ( this.quest.item.raw < 0 ){
            work_stat.fatigue += 1
            echo(events,"you broke the "+this.quest.item.name,"p")
            echo(events,"you start working on a new "+this.quest.item.name,"p")
            this.quest.item.quality = 0;
            this.quest.item.raw = 100
        }else{
            if ( this.quest.item.raw > work_stat.force ) {
                echo(events," the "+this.quest.item.name+" seems still suitable for work",'p')
            }else{
                echo(events," the "+this.quest.item.name+" seems refined enough",'p')
            }
        }
        this.timer = 0
        input.value = this.id+"0 "
    }.bind(this),time*1000)
    input.value = this.id+"1 "
}

Order.prototype.hammer = function(str){
    if(!this.held || !str) {
        work_stat.fatigue += 1
        echo(workshop,this.id + " The "+ this.quest.item.name +" is improperly held",'p')
        echo(events,"You miss the " + this.quest.item.name + " with your hammer","p")
        echo(workshop,"Fail "+this.quest.item.name +" quality - "+prog(this.timer),"p")
        this.quest.item.quality -= prog(this.timer)
        return
    }
    if(str == this.holder){
        work_stat.fatigue += 1
        this.quest.item.quality+= prog(this.timer)
        echo(events,"You strike the " + this.quest.item.name + " with your hammer","p")
        echo(workshop,"Success "+this.quest.item.name +" quality + "+prog(this.timer),"p")
        anim(center,"hammer",2)
        this.hold()
    }else{
        work_stat.fatigue += 1
        this.quest.item.quality -= (1 + work_stat.force - this.timer)
        echo(events,"You miss the " + this.quest.item.name + " with your hammer","p")
        echo(workshop,"Fail "+this.quest.item.name +" quality - "+prog(this.timer),"p")
        this.hold()
    }
}

Order.prototype.deliver = function(){
    echo(events,"You proudly present the "+this.quest.item.name+" to "+this.quest.client.pnj.name,"p")
    if(this.quest.item.raw - this.quest.item.quality > 0){
        echo(events,this.quest.client.pnj.name + " refuse your badly worked " + this.quest.item.name + " and broke it","p")
            echo(events,"you start working on a new "+this.quest.item.name,"p")
            this.quest.item.quality = 0;
            this.quest.item.raw = 100
        return this.quest.client.pnj.reputation -= 1
    }
    this.quest.client.pnj.reputation += 1
        echo(events,this.quest.client.pnj.name + " accept your " + this.quest.item.name,"p")
    anim(center,"yes",8,33)
    echo(events,this.quest.client.pnj.name+" is pleased","p")
    echo(workshop,this.id + " removed from orders","p")
    this.removed = "delivered"
    choices[this.id] = undefined
    choices[this.id + '0'] = undefined
    choices[this.id + '1'] = undefined
    choices[this.id + '2'] = undefined
    this.quest.client.pnj.item = this.quest.item
}
// promise({
//     wood : 5,
//     leonidas_spear : fail,
// })
