# Game Plot Summary

You are a mortal blacksmith in the ancient Greece. Through your hard work you gain a little renown of your own that catch the eye of some powerfull man. Kings, great warriors or even heroes come to you, asking for your expertise.

You have to make weapons and armors for them. Helping them in their quests of glory and gaining more and more fame. But the weapons business is a dangerous one! Some people can come to you, paying you to botch your own work or even order you to not finish your order! It's up to you to choose wisely who you help and who you betray.

At some point you will have the attention of the gods themselves who can direct you to build some wonderful artefacts.